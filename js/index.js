import Modal from './components/Modal.js';

const loginModal = new Modal();
loginModal.render();

function greater(token) {
  fetch('https://ajax.test-danit.com/api/v2/cards', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
    body: JSON.stringify({
      title: 'Визит к кардиологу',
      description: 'Плановый визит',
      doctor: 'Cardiologist',
      bp: '24',
      age: 25,
      weight: 70,
    }),
  })
    .then((response) => response.json())
    .then((response) => console.log(response));
}

// greater('825217a0-e755-483d-bfea-a9067600a2f3');

function getAllCards(token) {
  fetch('https://ajax.test-danit.com/api/v2/cards', {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
  })
    .then((response) => response.json())
    .then((response) => console.log(response));
}

getAllCards('825217a0-e755-483d-bfea-a9067600a2f3');
